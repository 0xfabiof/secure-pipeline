#!/bin/bash

sleep 10

curl tomcat:8080/webapp/ -m 5

if [ $? -eq 0 ]
then
	echo "Connection successful"
    exit 0
else
 	echo "Couldnt reach endpoint - is runners IP allowed in servers security group?"
	exit 1
fi
