old_d=$(pwd)
d=$(dirname $0)
cd $d
export SONARQUBE_HOME=$(pwd)
cd $old_d
docker run --rm -it \
  -v $SONARQUBE_HOME/conf:/opt/sonarqube/conf \
  -v $SONARQUBE_HOME/extensions:/opt/sonarqube/extensions \
  -v $SONARQUBE_HOME/data:/opt/sonarqube/data \
  -v $SONARQUBE_HOME/logs:/opt/sonarqube/logs \
  --entrypoint "/bin/bash" \
  sonarqube
