import json
import sys

def parseSAST(file):
    ## SAST Parser Module Compatible with Sonarqube SAST Report
    with open(file,'r') as file:
        sast_string = file.read()

    sast_json = json.loads(sast_string)
    issues = len(sast_json['issues'])
    
    return issues


def parseDAST(file):
    with open(file,'r') as file:
        dast_string = file.read()
    
    dast_json = json.loads(dast_string)
    issues = len(dast_json['site'][0]['alerts'])

    return issues

def parseSecrets(file):
    ## Secrets Scan Parser Module Compatible with Gitleaks Report
    with open(file,'r') as file:
        secrets_string = file.read()
    
    secrets_json = json.loads(secrets_string)
    
    ## Filter 'file'='.gitleaks.toml' - Scanner is flagging its own rules as secrets
    secrets_json_filtered = [x for x in secrets_json if x['file']!='.gitleaks.toml']
    issues = len(secrets_json_filtered)
    
    return issues

def parseContainer(file):
    ## Container Scan Parser Module Compatible with Clair-scanner Report
    with open(file,'r') as file:
        container_string = file.read()
    
    container_json = json.loads(container_string)
    issues = len(container_json['vulnerabilities'])
    
    return issues

def parseDependencies(file):
    ## Dependency Scan Parser Module Compatible with Owasp Dependency Check Json Report
    with open(file,'r') as file:
        dependency_string = file.read()
    
    dependency_json = json.loads(dependency_string)
    issues = 0
    for i in dependency_json['dependencies']:
        if i.get('vulnerabilities'):
            issues += len(i['vulnerabilities'])

    return issues


def color_calculator(number):
    if number>95:
        return 'red'
    if number>45:
        return 'orange'
    else:
        return 'green'

def buildDashboard(repo_name,build_no,sast_issues,secrets_issues,container_issues,dast_issues,dependency_issues,total_issues):
    color_sast=color_calculator(sast_issues)
    color_secrets=color_calculator(secrets_issues)
    color_container=color_calculator(container_issues)
    color_dast=color_calculator(dast_issues)
    color_dependency=color_calculator(dependency_issues)
    color_total=color_calculator(total_issues)

    page = '''
    <html>
        <style>
            th {
                height: 70px;
                width: 100px;
                text-align: center;
                border: 1px solid black;
            }
            td {
                height: 70px ;
                width: 100px ;
                text-align: center;
                border: 1px solid black;
            }

            body {
                margin-top: 3em;
            }
            td.button:hover {
                background: gray;
                color: white;    
            }
        </style>
        <body>
            <h1 align="center">%s</h1>
            <h4 align="center">Commit: %s</h2>
            <br>
            <h1 align="center">Issues:</h1>
            <table align="center">
                <tr>
                    <th>SAST</th>
                    <th>Secrets</th>
                    <th>Container</th>
                    <th>DAST</th>
                    <th>Dependencies</th>
                </tr>
                <tr>
                    <td style="background:%s;color:white;"><b>%d</b></td>
                    <td style="background:%s;color:white;"><b>%d</b></td>
                    <td style="background:%s;color:white;"><b>%d</b></td>
                    <td style="background:%s;color:white;"><b>%d</b></td>
                    <td style="background:%s;color:white;"><b>%d</b></td>
                </tr>
                <tr>
                    <td class="button" style ="cursor:pointer" onclick="location.href='reports/report_sast.json'">Details</td>
                    <td class="button" style ="cursor:pointer" onclick="location.href='reports/report_secrets.json'">Details</td>
                    <td class="button" style ="cursor:pointer" onclick="location.href='reports/report_container-scan.json'">Details</td>
                    <td class="button" style ="cursor:pointer" onclick="location.href='reports/report_dast.json'">Details</td>
                    <td class="button" style ="cursor:pointer" onclick="location.href='reports/report_dependency.json'">Details</td>


                </tr>
            </table>
            <br>
            <br>
            <h2 align="center">Total Issues:</h1>
            <h1 style="color:%s" align="center">%d</h1>
        </body>
    </html>
    ''' % (repo_name,build_no,color_sast,sast_issues,color_secrets,secrets_issues,color_container,container_issues,color_dast,dast_issues,color_dependency,dependency_issues,color_total,total_issues)
    with open('dashboard.html','w') as file:
        file.write(page)

try:
    sast_issues = parseSAST('reports/report_sast.json')
    secrets_issues = parseSecrets('reports/report_secrets.json')
    container_issues = parseContainer('reports/report_container-scan.json')
    dast_issues = parseDAST('reports/report_dast.json')
    dependency_issues = parseDependencies('./reports/report_dependency.json')

    total_issues = sast_issues+secrets_issues+container_issues+dast_issues+dependency_issues
    repo_name = sys.argv[1]
    build_no = sys.argv[2]

    buildDashboard(repo_name, build_no, sast_issues,secrets_issues,container_issues,dast_issues,dependency_issues,total_issues)
except:
    pass