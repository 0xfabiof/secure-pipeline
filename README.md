# Secure Pipeline
## Java Project Template
(using OWASP's [JavaVulnerableLab](https://github.com/CSPF-Founder/JavaVulnerableLab) as the repository sample)

Secure Pipeline Project is a DevSecOps pipeline built around open-source security tools that proposes to integrate as many security checks as possible into a software delivery pipeline.

The end-goal would be to have a way of consolidating different sources of finding security issues in an application to reach a weighted overall risk score for a given project/build

As a proof of concept, the build_risk_calculator.py script in this repository parses the json output report of each of the tools and produces a dynamically generated html report of the findings of each security validation in the pipeline:

![Results Dashboard Proof of Concept](https://i.imgur.com/lm4ABl5.png "Results Dashboard Proof of Concept")

As of right now, the pipeline integrates 4 types of security checks - each of them independent of the others that can be fully implemented in any project of the type, as long as the following requirements are met:

## Dynamic Analysis Security Testing (DAST):

Leverages [OWASP ZAP](https://owasp.org/www-project-zap/) tool to run a series of crawling/spidering/fuzzing tests on a web-application

Requires:
* Web Application Project Deployed to a Web-server
    * This web-server should be exposed to the server from which the ZAP tool will be running - if using a QA/Test Web-server (recommended), the network flows should be open  
* Runner/Slave with the following software and respective dependencies installed:
    * OWASP Zed Attack Proxy - available [here](https://www.zaproxy.org/download/)
    * ZAP-Cli Binary (A commandline tool that wraps the OWASP ZAP API) - available [here](https://github.com/Grunny/zap-cli)

## Static Analysis Security Testing (SAST)

Utilizes [Sonarqube](https://www.sonarqube.org/)'s Static Analysis functionality and respective API to run static code analysis for vulnerabilities in the source-code of the application:

Requires:
* Sonarqube Project Config File 'sonar-project.properties' in repository
* Runner/Slave with the following software and respective dependencies installed:
    * Sonarqube Server - available [here](https://www.sonarqube.org/downloads/)
    * SonarScanner (in this cased used maven's goal) - available [here](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/)

## Container Security

Takes advantage of the [Clair](https://github.com/quay/clair) tool - a server to index Common Vulnerabilities Exposure (CVEs) for Container Images - and submits the image resulting from the containerization of the project's application to this database.

Requires:
* A docker image resultant from the project pushed to a registry - sample for this project available [here](https://hub.docker.com/r/0xfabiof/secure-pipeline-java)
* Runner/Slave with the following software and respective dependencies installed:
    * Docker - available [here](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    * Clair-local-scan Docker Image - available [here](https://hub.docker.com/r/arminc/clair-local-scan)
    * Clair DB Docker Image - available [here](https://hub.docker.com/r/arminc/clair-db)
    * Clair-scanner Binary (a CLI tool to submit images from an arbitrary registry to a Clair-server scan) - available [here](https://github.com/arminc/clair-scanner)

## Secrets Checker

Leverages the [Git Leaks](https://github.com/zricethezav/gitleaks) tool - a highly customizable and scalable tool to scan code repositories for hardwired/exposed credentials such as passwords, ssh keys, api keys, PII, etc using regex rules.

Requires:
* Runner/Slave with the following software and respective dependencies installed:
    * Gitleaks Binary - available [here](https://github.com/zricethezav/gitleaks)
    * Config file with regex rules - various samples available [here](https://github.com/zricethezav/gitleaks/tree/master/examples)
